#include <iostream>
#include <string>
using namespace std;

#include "Person.h"

//base constructor
Person::Person() {
    lastName = "Unknown";
    firstName = "Unknown";
    address = "Unknown";
    city = "Unknown";
    state = "Unknown";
    phone = -1;
    zip = -1;
}

//other useful constructors
Person::Person(string lName, string fName, string add, string cit, string sta, int phn, int zipCode) {
    lastName = lName;
    firstName = fName;
    address = add;
    city = cit;
    state = sta;
    phone = phn;
    zip = zipCode;
}

Person::Person(string lName, string fName) {
    lastName = lName;
    firstName = fName;
    address = "Unknown";
    city = "Unknown";
    state = "Unknown";
    phone = -1;
    zip = -1;
}

//mutator functions
void Person::SetLastName(string lName){
    lastName = lName;
}

void Person::SetFirstName(string fName){
    firstName = fName;
}

void Person::SetAddress(string add){
    address = add;
}

void Person::SetCity(string cit){
    city = cit;
}

void Person::SetState(string sta){
    state = sta;
}

void Person::SetPhoneNumber(int phn){
    phone = phn;
}

void Person::SetZipCode(int zipCode){
    zip = zipCode;
}

//sets all Person class variables at once
void Person::SetAllPerson(string lName, string fName, string add, string cit, string sta, int phn, int zipCode){
    lastName = lName;
    firstName = fName;
    address = add;
    city = cit;
    state = sta;
    phone = phn;
    zip = zipCode;
}