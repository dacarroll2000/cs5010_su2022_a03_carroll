#ifndef CUSTOMER_H
#define CUSTOMER_H

class Customer : public Person {
    public:
        //constructor functions
        Customer();
        Customer(string lName, string fName, string add, string cit, string sta, int phn, int zipCode, string emailAddress, bool mailing, string customerNum);
        Customer(string lName, string fName, string add, string cit, string sta, int phn, int zipCode, string emailAddress, bool mailing);
        Customer(string lName, string fName, string add, string cit, string sta, int phn, int zipCode);
        Customer(string lName, string fName, string emailAddress, bool mailing);
        Customer(string lName, string fName);
        //mutator functions
        void SetCustomerNumber(string customerNum);
        void SetEmail(string emailAddress);
        void SetMailingList(bool mailing);
        void SetAllCustomer(string lName, string fName, string add, string cit, string sta, int phn, int zipCode, string emailAddress, bool mailing, string customerNum);
        //inline static variable, so there is no need to declare it elsewhere (c++ 17 feature)
        inline static int latestCustomerNumber;
        //variables left public for easy print function
        string email;
        bool mailingList;
        string customerNumber;
    private:
        string GetCustomerNumber();
};

#endif