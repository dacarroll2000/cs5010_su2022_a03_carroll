I had issues with Visual Studio / the compiler where using vectors would cause the program to not compile. I was finally able to solve this issue by switching my IDE to CLion.
I needed to put the entire path into main.cpp because of how CLion handles files. Please add your own file path into main.cpp while testing the code.
The output of main.cpp is below:

| Last Name   | First Name  | Address  | City   | State   | Phone     | Zip  | Email              | Mailing List  | Customer Number  |
--------------------------------------------------------------------------------------------------------------------------------------
| Carroll     | David       | 1234     | Grown  | Alaska  | 2147483647| 12345| dacarro@bgsu.edu   | True          | 1                |
| Moris       | Hank        | 4321     | Low    | Ohio    | 2147483647| 54321| Unknown            | True          | 2                |
| Mackle      | Alice       | 987      | Perry  | New     | 2147483647| 58205| Unknown            | False         | 3                |
| Gordon      | Julie       | Unknown  | Unknown| Unknown | Unknown   | -1   | Unknown            | False         | 4                |
| Millhouse   | Daniel      | 5735     | Hudson | Hawaii  | 2147483647| 58365| DanMill@gmail.com  | True          | 5                |
| Hilston     | Rose        | 5745     | Perry  | Virginia| 2147483647| 56249| Grillby@yahoo.com  | False         | 6                |
| Blue        | Paris       | Unknown  | Unknown| Unknown | Unknown   | -1   | Unknown            | False         | 7                |
| Grave       | Teddy       | Unknown  | Unknown| Unknown | Unknown   | -1   | Unknown            | False         | 8                |
| Crate       | Mavis       | 5739     | Akron  | Ohio    | 2147483647| 38571| Unknown            | False         | 9                |
| Smith       | Teddrocky   | Unknown  | Unknown| Unknown | Unknown   | -1   | Unknown            | False         | 10               |
| Imre        | Forest      | 0783     | New    | New     | 2147483647| 57925| mavis@hotmail.com  | True          | 11               |
| Watson      | Monet       | Unknown  | Unknown| Unknown | Unknown   | -1   | Unknown            | False         | 12               |
| Walsh       | Bill        | 598      | Loris  | Michigan| 2147483647| 17490| Unknown            | False         | 13               |
| Libson      | Ratchet     | 385      | Mill   | Navada  | 2147483647| 94682| Fandom@google.com  | False         | 14               |
| Boris       | Noami       | Unknown  | Unknown| Unknown | Unknown   | -1   | Unknown            | False         | 15               |
| Landover    | Harry       | 4912     | Hill   | New     | 1593824816| 97816| Unknown            | False         | 16               |
| Harris      | Emma        | Unknown  | Unknown| Unknown | Unknown   | -1   | Unknown            | False         | 17               |
| Allen       | Barry       | 7892     | Groovy | Maine   | 2147483647| 48216| Groovster@gmail.com| True          | 18               |
| Loris       | Garry       | 4186     | Old    | Oregon  | 2147483647| 97628| Garry@hotmail.com  | False         | 19               |
| Unknown     | Unknown     | Unknown  | Unknown| Unknown | Unknown   | -1   | Unknown            | False         | 20               |
satisfied?
