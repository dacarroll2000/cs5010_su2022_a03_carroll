#include <iostream>
#include <string>
using namespace std;

#include "Person.h"
#include "Customer.h"

Customer::Customer() : Person() {
    //only need to set the Customer variables here, the Person class sets the remaining variables
    customerNumber = GetCustomerNumber();
    email = "Unknown";
    mailingList = false;
}

//pass values to the Person class constructor
Customer::Customer(string lName, string fName, string add, string cit, string sta, int phn, int zipCode, string emailAddress, bool mailing, string customerNum) : Person(lName, fName, add, cit, sta, phn, zipCode) {
    customerNumber = customerNum;
    email = emailAddress;
    mailingList = mailing;
}

Customer::Customer(string lName, string fName, string add, string cit, string sta, int phn, int zipCode, string emailAddress, bool mailing) : Person(lName, fName, add, cit, sta, phn, zipCode) {
    customerNumber = GetCustomerNumber();
    email = emailAddress;
    mailingList = mailing;
}

Customer::Customer(string lName, string fName, string add, string cit, string sta, int phn, int zipCode) : Person(lName, fName, add, cit, sta, phn, zipCode) {
    customerNumber = GetCustomerNumber();
    email = "Unknown";
    mailingList = false;
}

Customer::Customer(string lName, string fName, string emailAddress, bool mailing) : Person(lName, fName) {
    customerNumber = GetCustomerNumber();
    email = emailAddress;
    mailingList = mailing;
}

Customer::Customer(string lName, string fName) : Person(lName, fName) {
    customerNumber = GetCustomerNumber();
    email = "Unknown";
    mailingList = false;
}

//mutator functions
void Customer::SetCustomerNumber(string customerNum){
    customerNumber = customerNum;
}

void Customer::SetEmail(string emailAddress){
    email = emailAddress;
}

void Customer::SetMailingList(bool mailing){
    mailingList = mailing;
}

//used to set all values at once
void Customer::SetAllCustomer(string lName, string fName, string add, string cit, string sta, int phn, int zipCode, string emailAddress, bool mailing, string customerNum) {
    //pass variables to the Person class SetAllPerson mutator function
    SetAllPerson(lName, fName, add, cit, sta, phn, zipCode);
    customerNumber = customerNum;
    email = emailAddress;
    mailingList = mailing;
}

//generates a new customer number
string Customer::GetCustomerNumber(){
    //temporary variable
    int customerNum;
    string customerNumStr;

    //get latest customerNumber + 1
    customerNum = 1 + latestCustomerNumber;
    ++latestCustomerNumber;

    //convert to string
    customerNumStr = to_string(customerNum);

   return customerNumStr;
}