#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <iomanip>
using namespace std;

#include "Person.h"
#include "Customer.h"


void PrintCustomers(vector<Customer> customers) {
    //variables for collecting the max length of all customer values
    // minimum value is large enough to fit column name
    int lastNameLength = 12;
    int firstNameLength = 12;
    int addressLength = 9;
    int cityLength = 6;
    int stateLength = 7;
    int phoneLength = 7;
    int zipLength = 5;
    int emailLength = 7;
    int customerNumberLength = 17;
    int mailingListLength = 14;

    //finds max length of all values
    for (int i = 0; i < customers.size(); ++i) {
        if (lastNameLength < customers.at(i).lastName.length()) {
            lastNameLength = customers.at(i).lastName.length();
        }
        if (firstNameLength < customers.at(i).firstName.length()) {
            firstNameLength = customers.at(i).firstName.length();
        }
        if (addressLength < customers.at(i).address.length()) {
            addressLength = customers.at(i).address.length();
        }
        if (cityLength < customers.at(i).city.length()) {
            cityLength = customers.at(i).city.length();
        }
        if (stateLength < customers.at(i).state.length()) {
            stateLength = customers.at(i).state.length();
        }
        if (phoneLength < to_string(customers.at(i).phone).length()) {
            phoneLength = to_string(customers.at(i).phone).length();
        }
        if (zipLength < to_string(customers.at(i).zip).length()) {
            zipLength = to_string(customers.at(i).zip).length();
        }
        if (emailLength < customers.at(i).email.length()) {
            emailLength = customers.at(i).email.length();
        }
        if (customerNumberLength < customers.at(i).customerNumber.length()) {
            customerNumberLength = customers.at(i).customerNumber.length();
        }
    }

    //header for output
    //+1/+2 is to account for characters at beginning of strings
    cout << setw(lastNameLength+2) << left << "| Last Name" << right << "|";
    cout << setw(firstNameLength+1) << left << " First Name" << right << "|";
    cout << setw(addressLength+1) << left << " Address" << right << "|";
    cout << setw(cityLength+1) << left << " City" << right << "|";
    cout << setw(stateLength+1) << left << " State" << right << "|";
    cout << setw(phoneLength+1) << left << " Phone" << right << "|";
    cout << setw(zipLength+1) << left << " Zip" << right << "|";
    cout << setw(emailLength+1) << left << " Email" << right << "|";
    cout << setw(mailingListLength+1) << left << " Mailing List" << right << "|";
    cout << setw(customerNumberLength+1) << left << " Customer Number" << "|" << endl;

    //create line of dashes
    cout << setfill('-') << setw(lastNameLength + firstNameLength + addressLength + cityLength + stateLength + phoneLength + zipLength + emailLength + mailingListLength + customerNumberLength + 21) << "-" << endl;

    //output contents of each Customer object
    for (int i = 0; i < customers.size(); ++i) {
        cout << setfill(' ') << setw(2) << "| " << setw(lastNameLength) << left << customers.at(i).lastName << right << "| ";
        cout << setw(firstNameLength) << left << customers.at(i).firstName << right << "| ";
        cout << setw(addressLength) << left << customers.at(i).address << right << "| ";
        cout << setw(cityLength) << left << customers.at(i).city << right << "| ";
        cout << setw(stateLength) << left << customers.at(i).state << right << "| ";
        //if statements used so unknown phone numbers output in a more understandable way
        if (customers.at(i).phone == -1) {
            cout << setw(phoneLength) << left << "Unknown" << right << "| ";
        }
        else {
            cout << setw(phoneLength) << left << customers.at(i).phone << right << "| ";
        }
        cout << setw(zipLength) << left << customers.at(i).zip << right << "| ";
        cout << setw(emailLength) << left << customers.at(i).email << right << "| ";
        //converts 0/1 into true or false for output
        if (customers.at(i).mailingList == 0) {
            cout << setw(mailingListLength) << left << "False" << right << "| ";
        }
        else {
            cout << setw(mailingListLength) << left << "True" << right << "| ";
        }
        cout << setw(customerNumberLength) << left << customers.at(i).customerNumber << "| " << endl;
    }
}


int main() {
    //setting static variable to 0
    Customer::latestCustomerNumber = 0;
    //stream variables
    ifstream inFS;
    //data to add / extract from file
    string lastName;
    string firstName;
    string address;
    string city;
    string state;
    int phone;
    int zip;
    string email;
    int intMailingList;
    bool mailingList;
    int customerNumber;
    //variables for extracting data from file
    string fileLine;
    string tmpString;
    int size;
    stringstream tmpStream;

    //other variables
    vector<Customer> customerVector;
    Customer tmpCustomer;
    
    // attempt to load in Customer.txt
    cout << "Opening file Customer.txt" << endl;

    //Needed to add the whole file path in order for new IDE (CLion) to use txt file
    inFS.open("c:/Users/dacar/Desktop/CS5010_class_projects/cs5010_su2022_a03_carroll/Customer.txt");
    if (!inFS.is_open()) {
        cout << "Could not open file Customer.txt." << endl;
        return 0;
    }
    
    // load in any data that may be in the file and find largest customerNumber
    cout << "Reading data from Customer.txt" << endl;

    //read in data from Customer.txt to create the vector of customers
    while (!inFS.eof()) {
        getline(inFS, fileLine);

        //Find the location of the first comma. The value returned will be the size of the substring that will be used in tmpString
        size = fileLine.find(',');
        //Sets tmpString to a substring of fileLine
        tmpString = fileLine.substr(0,size);
        //sends tmpString into tmpStream, so it can be read out into the variable correctly (this isn't necessary for all the variables, but is used on all of them anyway to keep the code consistent)
        tmpStream.str(tmpString);
        //sets the variable with the contents of tmpStream. getline is used for strings, >> is used for everything else
        getline(tmpStream, lastName);
        //clear tmpStream
        tmpStream.clear();
        //erase fileLine up to the next value
        fileLine.erase(0,size+2);

        size = fileLine.find(',');
        tmpString = fileLine.substr(0,size);
        tmpStream.str(tmpString);
        getline(tmpStream, firstName);
        tmpStream.clear();
        fileLine.erase(0,size+2);

        size = fileLine.find(',');
        tmpString = fileLine.substr(0,size);
        tmpStream.str(tmpString);
        getline(tmpStream, address);
        tmpStream.clear();
        fileLine.erase(0,size+2);

        size = fileLine.find(',');
        tmpString = fileLine.substr(0,size);
        tmpStream.str(tmpString);
        getline(tmpStream, city);
        tmpStream.clear();
        fileLine.erase(0,size+2);

        size = fileLine.find(',');
        tmpString = fileLine.substr(0,size);
        tmpStream.str(tmpString);
        getline(tmpStream, state);
        tmpStream.clear();
        fileLine.erase(0,size+2);

        size = fileLine.find(',');
        tmpString = fileLine.substr(0,size);
        tmpStream.str(tmpString);
        tmpStream >> phone;
        tmpStream.clear();
        fileLine.erase(0,size+2);

        size = fileLine.find(',');
        tmpString = fileLine.substr(0,size);
        tmpStream.str(tmpString);
        tmpStream >> zip;
        tmpStream.clear();
        fileLine.erase(0,size+2);

        size = fileLine.find(',');
        tmpString = fileLine.substr(0,size);
        tmpStream.str(tmpString);
        getline(tmpStream, email);
        tmpStream.clear();
        fileLine.erase(0,size+2);

        size = fileLine.find(',');
        tmpString = fileLine.substr(0,size);
        tmpStream.str(tmpString);
        tmpStream >> customerNumber;
        tmpStream.clear();
        fileLine.erase(0,size+2);

        size = fileLine.find(',');
        tmpString = fileLine.substr(0,size);
        tmpStream.str(tmpString);
        tmpStream >> intMailingList;
        tmpStream.clear();
        fileLine.erase(0,size+2);

        //Booleans are stored as 1/0 in Customer.txt, so this if-statement is used to set the mailingList boolean correctly
        if (intMailingList == 0) {
            mailingList = false;
        }
        else {
            mailingList = true;
        }

        //Change the values of the temporary customer object, and push this temporary object to the back of customerVector
        tmpCustomer.SetAllCustomer(lastName, firstName, address, city, state, phone, zip, email, mailingList, to_string(customerNumber));
        customerVector.push_back(tmpCustomer);

        //Used to set latestCustomerNumber to the largest customer Number of those read in from Customer.txt
        if (Customer::latestCustomerNumber < customerNumber) {
            Customer::latestCustomerNumber = customerNumber;
        }
    }
    if (!inFS.eof()) {
        cout << "Input failure before reaching end of file." << endl;
    }
    
    PrintCustomers(customerVector);

    //added a cin statement so the program does not terminate until wanted
    cout << "satisfied?" << endl;
    cin >> tmpString;
    return 0;
}