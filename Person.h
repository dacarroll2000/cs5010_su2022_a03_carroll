#ifndef PERSON_H
#define PERSON_H

class Person {
    public:
        //constructor functions
        Person();
        Person(string lName, string fName, string add, string cit, string sta, int phn, int zipCode);
        Person(string lName, string fName);
        //mutator functions
        void SetLastName(string lName);
        void SetFirstName(string lName);
        void SetAddress(string add);
        void SetCity(string cit);
        void SetState(string Sta);
        void SetPhoneNumber(int phn);
        void SetZipCode(int zipCode);
        void SetAllPerson(string lName, string fName, string add, string cit, string sta, int phn, int zipCode);
        //variables left public for easy print function
        string lastName;
        string firstName;
        string address;
        string city;
        string state;
        int phone;
        int zip;
};

#endif